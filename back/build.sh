#!/usr/bin/env bash

set -e

based_tag_name=${CI_REGISTRY_IMAGE:-yassir/zira}

mvn clean package
docker build -t "$based_tag_name/back:latest" .