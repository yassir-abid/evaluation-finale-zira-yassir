package com.zenika.zira.project;

import com.zenika.zira.project.domain.Project;
import com.zenika.zira.web.exceptions.InvalidException;
import org.junit.jupiter.api.Test;

import java.time.Year;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class ProjectTest {

    @Test
    void should_create_project() {
        Project project = new Project();
        project.setName("Kaameloott");
        project.setDescription("API pour des citations aléatoires");
        project.setAuthor("chuck");
        assertThat(project).isNotNull();
        assertThat(project.getName()).isEqualTo("Kaameloott");
        assertThat(project.getDescription()).isEqualTo("API pour des citations aléatoires");
    }

    @Test
    void should_reject_empty_name() {
        assertThatThrownBy(() -> {
            Project project = new Project();
            project.setName("");
            project.setDescription("API pour des citations aléatoires");
        }).isInstanceOf(InvalidException.class);
    }

    @Test
    void should_reject_empty_description() {
        assertThatThrownBy(() -> {
            Project project = new Project();
            project.setName("Kaameloott");
            project.setDescription("");
        }).isInstanceOf(InvalidException.class);
    }

    @Test
    void should_reject_description_with_length_greater_than_500_characters() {
        assertThatThrownBy(() -> {
            Project project = new Project();
            project.setName("Kaameloott");
            project.setDescription("""
                    Fusce nec tincidunt metus, sit amet auctor diam. Sed at facilisis tortor, 
                    quis vulputate justo. Praesent tortor nulla, congue ac purus sit amet, tempor efficitur nulla. 
                    Aliquam cursus varius ante, quis egestas justo. Integer molestie tellus magna, 
                    non porta elit posuere ac. Pellentesque lorem turpis, suscipit lacinia justo eget, 
                    porta feugiat magna. Fusce tincidunt a mauris vel egestas. Proin laoreet diam et ipsum 
                    aliquet ornare. Sed ultricies, nulla et feugiat rutrum, lorem risus ultrices erat, 
                    quis ultrices nisi orci ac lectus. Vivamus sagittis libero volutpat, vestibulum tellus quis.
                    """);
        }).isInstanceOf(InvalidException.class);
    }

}
