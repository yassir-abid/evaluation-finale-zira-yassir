package com.zenika.zira.project;

import com.zenika.zira.ticket.domain.Ticket;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@Sql("classpath:database/clean.sql")
public class ProjectRessourceTest {
    @LocalServerPort
    int port;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @BeforeEach
    public void setup() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = this.port;
        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
    }

    @Test
    void should_create_project_when_authenticated_with_right_role() {
        // Create project
        final Integer projectId =
                RestAssured
                        .given()
                        .body("""
                                   {
                                       "name": "kaamelott",
                                       "description": "Projet pour mettre en place une api de citations de kaamelott"
                                   }
                                """)
                        .when()
                        .header("content-type", "application/json")
                        .header("Authorization", "Basic Y2h1Y2s6cGFzc3dvcmQ=")
                        .post("/api/projects")
                        .then()
                        .statusCode(200)
                        .extract().path("id")
                ;

        RestAssured
                .when()
                .get("/api/projects/{id}", projectId)
                .then()
                .statusCode(200)
                .body("id", is(projectId))
                .body("name", is("kaamelott"))
                .body("description", is("Projet pour mettre en place une api de citations de kaamelott"))
        ;

        final Integer projectTotal = this.jdbcTemplate.queryForObject("SELECT COUNT(*) FROM PROJECT", Integer.class);
        assertThat(projectTotal).isEqualTo(1);
    }

    @Test
    void should_return_unauthorized_http_status_when_creating_project_without_being_authenticated() {
        // Create project
                RestAssured
                        .given()
                        .body("""
                                   {
                                       "name": "kaamelott",
                                       "description": "Projet pour mettre en place une api de citations de kaamelott"
                                   }
                                """)
                        .when()
                        .header("content-type", "application/json")
                        .post("/api/projects")
                        .then()
                        .statusCode(401)
                ;
    }

    @Test
    void should_return_forbidden_http_status_when_creating_project_with_wrong_role() {
        // Create project
        RestAssured
                .given()
                .body("""
                                   {
                                       "name": "kaamelott",
                                       "description": "Projet pour mettre en place une api de citations de kaamelott"
                                   }
                                """)
                .when()
                .header("content-type", "application/json")
                // authenticated as visitor
                .header("Authorization", "Basic dXNlcjpwYXNzd29yZA==")
                .post("/api/projects")
                .then()
                .statusCode(403)
        ;
    }

    @Test
    void should_update_project_when_authenticated_with_right_role() {
        // Create project
        final Integer projectId =
                RestAssured
                        .given()
                        .body("""
                                   {
                                       "name": "kaamelott",
                                       "description": "Projet pour mettre en place une api de citations de kaamelott"
                                   }
                                """)
                        .when()
                        .header("content-type", "application/json")
                        // authenticated as user
                        .header("Authorization", "Basic Y2h1Y2s6cGFzc3dvcmQ=")
                        .post("/api/projects")
                        .then()
                        .statusCode(200)
                        .extract().path("id")
                ;

        RestAssured
                .when()
                .get("/api/projects/{id}", projectId)
                .then()
                .statusCode(200)
                .body("id", is(projectId))
                .body("name", is("kaamelott"))
                .body("description", is("Projet pour mettre en place une api de citations de kaamelott"))
        ;

        // Update project
        RestAssured
                .given()
                .body("""
                           {
                               "name": "API kaamelott",
                               "description": "Gérer des citations de kaamelott"
                           }
					    """)
                .when()
                .header("content-type", "application/json")
                // authenticated as user
                .header("Authorization", "Basic Y2h1Y2s6cGFzc3dvcmQ=")
                //.accept("text/plain")
                .put("/api/projects/{id}", projectId)
                .then()
                .statusCode(200)
        ;

        RestAssured
                .with().header("accept", "application/json")
                .when()
                .get("/api/projects/{id}", projectId)
                .then()
                .statusCode(200)
                .body("id", is(projectId))
                .body("name", is("API kaamelott"))
                .body("description", is("Gérer des citations de kaamelott"))

        ;

        final Integer projectTotal = this.jdbcTemplate.queryForObject("SELECT COUNT(*) FROM PROJECT", Integer.class);
        assertThat(projectTotal).isEqualTo(1);
    }

    @Test
    void should_add_ticket_to_project_when_authenticated_with_right_role() {
        // Create project
        final Integer projectId =
                RestAssured
                        .given()
                        .body("""
                                   {
                                       "name": "kaamelott",
                                       "description": "Projet pour mettre en place une api de citations de kaamelott"
                                   }
                                """)
                        .when()
                        .header("content-type", "application/json")
                        // authenticated as user
                        .header("Authorization", "Basic Y2h1Y2s6cGFzc3dvcmQ=")
                        .post("/api/projects")
                        .then()
                        .statusCode(200)
                        .extract().path("id")
                ;

        RestAssured
                .when()
                .get("/api/projects/{id}", projectId)
                .then()
                .statusCode(200)
                .body("id", is(projectId))
                .body("name", is("kaamelott"))
                .body("description", is("Projet pour mettre en place une api de citations de kaamelott"))
        ;

        // add ticket
        final Integer ticketId = RestAssured
                .given()
                .body("""
                            {
                                "title": "Ajout citation",
                                "content": "Formulaire d'ajout d'une citation",
                                "status": "TODO"
                            }
					    """)
                .when()
                .header("content-type", "application/json")
                // authenticated as user
                .header("Authorization", "Basic Y2h1Y2s6cGFzc3dvcmQ=")
                //.accept("text/plain")
                .post("/api/projects/{id}/tickets", projectId)
                .then()
                .statusCode(200)
                .extract().path("id")
        ;

        RestAssured
                .with().header("accept", "application/json")
                .when()
                .get("/api/tickets/{id}", ticketId)
                .then()
                .statusCode(200)
                .body("title", is("Ajout citation"))
                .body("content", is("Formulaire d'ajout d'une citation"))
                .body("status", is("TODO"))
        ;

        final List<Ticket> tickets = RestAssured
                .when()
                .get("/api/projects/{id}", projectId)
                .then()
                .statusCode(200)
                .extract().path("tickets");

        final Integer projectTotal = this.jdbcTemplate.queryForObject("SELECT COUNT(*) FROM PROJECT", Integer.class);
        final Integer ticketTotal = this.jdbcTemplate.queryForObject("SELECT COUNT(*) FROM TICKET", Integer.class);
        assertThat(tickets.size()).isEqualTo(1);
        assertThat(projectTotal).isEqualTo(1);
        assertThat(ticketTotal).isEqualTo(1);
    }
}
