package com.zenika.zira.project;

import com.zenika.zira.project.domain.Project;
import com.zenika.zira.project.repository.ProjectRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class ProjectRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProjectRepository repository;

    @Test
    void should_create_project_in_database() {
        // Given
        final Project project = new Project();
        project.setName("Kaameloott");
        project.setDescription("Afficher des citations aléatoires de Kaameloott");
        project.setAuthor("chuck");
        project.setCreatedAt(Instant.now());

        // When
        final Project result = this.repository.save(project);
        this.entityManager.flush();

        // Then
        assertThat(result.getId()).isNotNull();
        assertThat(result.getDescription()).isEqualTo(project.getDescription());
    }

    @Test
    void should_get_project_by_id_from_database() {
        // Given
        final Project project = new Project();
        project.setName("Kaameloott");
        project.setDescription("Afficher des citations aléatoires de Kaameloott");
        project.setAuthor("chuck");
        project.setCreatedAt(Instant.now());

        this.entityManager.persistAndFlush(project);
        this.entityManager.clear();

        // When
        final Project result = this.repository.findById(project.getId()).orElse(null);

        // Then
        assertThat(result.getName()).isEqualTo(project.getName());
        assertThat(result.getDescription()).isEqualTo(project.getDescription());
    }
}
