package com.zenika.zira.project;

import com.zenika.zira.project.application.dto.ProjectInputDto;
import com.zenika.zira.project.domain.Project;
import com.zenika.zira.project.domain.ProjectService;
import com.zenika.zira.project.repository.ProjectRepository;
import com.zenika.zira.ticket.domain.TicketService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.any;
public class ProjectServiceTest {
    private ProjectService projectService;

    private final ProjectRepository projectRepository = Mockito.mock(ProjectRepository.class);

    private final TicketService mockTicketService = Mockito.mock(TicketService.class);

    @BeforeEach
    void before() {
        this.projectService = new ProjectService(
                this.projectRepository,
                this.mockTicketService
        );
    }

    @Test
    void should_create_project() {
        ProjectInputDto project = new ProjectInputDto();
        project.setName("kaamelott");
        project.setDescription("Projet pour mettre en place une api de citations de kaamelott");
        // Given
        Mockito.when(this.projectRepository.save(any()))
                .thenReturn(new Project().setId(2L));

        // When
        final long resultId = this.projectService.createProject(project, "chuck").getId();

        // Then
        Assertions.assertEquals(2L, resultId);
        Mockito.verify(this.projectRepository).save(any());
    }
}
