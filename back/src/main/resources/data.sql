BEGIN;

--- Project
INSERT INTO project (name, description, author, created_at)
VALUES ('Zira', 'Projet pour la gestion des tâches d''un projet', 'chuck', '2023-04-24 14:34:02.284312 +00:00')
ON CONFLICT DO NOTHING;

INSERT INTO project (name, description, author, created_at)
VALUES ('Pet Clinic', 'Projet pour la gestion de cliniques vétérinaires', 'brad', '2023-04-23 14:34:02.284312 +00:00')
ON CONFLICT DO NOTHING;

--- Reviews
INSERT INTO ticket (title, content, status, author, project_id)
VALUES ('Database', 'Initialiser une base de données postgres', 'DONE', 'leonardo', 1)
ON CONFLICT DO NOTHING;

INSERT INTO ticket (title, content, author, project_id)
VALUES ('CI', 'Mettre en place les pipelines CI','brad', 1)
ON CONFLICT DO NOTHING;

INSERT INTO ticket (title, content, status, author, project_id)
VALUES ('Formulaire', 'Ajouter la page du formulaire de modification de pet clinic', 'TODO', 'leonardo', 2)
ON CONFLICT DO NOTHING;

INSERT INTO ticket (title, content, status, author, project_id)
VALUES ('CRUD', 'Mettre en place le CRUD', 'INPROGRESS', 'brad', 2)
ON CONFLICT DO NOTHING;

COMMIT;


