package com.zenika.zira.project.application;

import com.zenika.zira.project.application.dto.ProjectDto;
import com.zenika.zira.project.application.dto.ProjectInputDto;
import com.zenika.zira.project.domain.ProjectService;
import com.zenika.zira.ticket.application.dto.TicketDto;
import com.zenika.zira.ticket.application.dto.TicketInputDto;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
@SecurityRequirement(name = "basicAuth")
public class ProjectController {

    private static final Logger logger = LoggerFactory.getLogger(ProjectController.class);
    private final ProjectService service;

    public ProjectController(ProjectService service) {
        this.service = service;
    }

    /**
     * get all projects sorted chronologically, and their tickets sorted by status and chronologically
     * @return list of all projects
     */
    @GetMapping()
    List<ProjectDto> getAllProjects(
            @RequestParam(required = false, defaultValue = "") String query,
            @RequestParam(required = false, defaultValue = "false") Boolean isUserAuthor
    ) {
        logger.info("getAllProjects");
        return this.service.getAllProjects(query, isUserAuthor);
    }

    /**
     * get one project by id, and its tickets sorted by status and chronologically
     * @param id project id
     * @return project or not found exception
     */
    @GetMapping("/{id}")
    ProjectDto getProjectById(@PathVariable("id") final long id) {
        logger.info("getProjectById");
        return this.service.getProjectById(id);
    }

    /**
     * create new project
     * @param projectInputDto project infos
     * @return project created
     */
    @PostMapping()
    ProjectDto createProject(
            @RequestBody @Validated ProjectInputDto projectInputDto,
            @AuthenticationPrincipal User user
    ) {
        logger.info("createProject");
        return this.service.createProject(projectInputDto, user.getUsername());
    }

    /**
     * update existing project
     * @param id project id to update
     * @param projectInputDto project infos
     * @return project updated
     */
    @PutMapping("/{id}")
    ProjectDto updateProject(
            @PathVariable Long id,
            @RequestBody @Validated ProjectInputDto projectInputDto
    ) {
        logger.info("updateProject");
        return this.service.updateProject(id, projectInputDto);
    }

    /**
     * delete project and associated tickets
     * @param id project id to delete
     */
    @DeleteMapping("/{id}")
    void deleteProject(@PathVariable Long id) {
        logger.info("deleteProject");
        this.service.deleteProject(id);
    }

    /**
     * create new ticket for a project
     * @param id project id
     * @param ticketInputDto ticket infos
     * @return ticket created
     */
    @PostMapping("/{id}/tickets")
    TicketDto addTicket(
            @PathVariable Long id,
            @RequestBody @Valid TicketInputDto ticketInputDto,
            @AuthenticationPrincipal User user
    ) {
        logger.info("addTicket");
        return this.service.addTicket(id, ticketInputDto, user.getUsername());
    }

}
