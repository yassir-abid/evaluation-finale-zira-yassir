package com.zenika.zira.project.application.dto;

import com.zenika.zira.project.domain.Project;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class ProjectInputDto {
    public static Project toDomain(ProjectInputDto projectInputDto) {
        Project project = new Project();
        project.setName(projectInputDto.getName());
        project.setDescription(projectInputDto.getDescription());
        return project;
    }

    @NotBlank
    private String name;

    @Size(max = 500)
    @NotBlank
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
