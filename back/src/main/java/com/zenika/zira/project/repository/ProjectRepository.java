package com.zenika.zira.project.repository;

import com.zenika.zira.project.domain.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProjectRepository extends JpaRepository<Project, Long> {
    List<Project> findAllByOrderByCreatedAtDesc();

    @Query(
            value = """
                        SELECT * FROM project 
                        WHERE name ILIKE CONCAT('%', :query, '%')
                        AND author ILIKE :author
                        ORDER BY created_at DESC;
                    """,
            nativeQuery = true,
            countQuery = """
                        SELECT COUNT(*) FROM project 
                        WHERE name ILIKE CONCAT('%', :query, '%')
                        AND author ILIKE :author
                        ORDER BY created_at DESC;
                    """
    )
    List<Project> findAllByNameAndAuthor(@Param("query") String query, @Param("author") String author );

    @Query(
            value = """
                        SELECT * FROM project 
                        WHERE name ILIKE CONCAT('%', :query, '%')
                        ORDER BY created_at DESC;
                    """,
            nativeQuery = true,
            countQuery = """
                        SELECT COUNT(*) FROM project 
                        WHERE name ILIKE CONCAT('%', :query, '%')
                        ORDER BY created_at DESC;
                    """
    )
    List<Project> findAllByName(@Param("query") String query);

    List<Project> findAllByNameIgnoreCaseContaining(String query);

    @Query(
            value = """
                        SELECT * FROM project 
                        WHERE author ILIKE :author
                        ORDER BY created_at DESC;
                    """,
            nativeQuery = true,
            countQuery = """
                        SELECT COUNT(*) FROM project 
                        WHERE author ILIKE :author
                        ORDER BY created_at DESC;
                    """
    )
    List<Project> findAllByAuthor(@Param("author") String author);
}
