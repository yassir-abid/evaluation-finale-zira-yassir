package com.zenika.zira.project.domain;

import com.zenika.zira.project.application.dto.ProjectDto;
import com.zenika.zira.project.application.dto.ProjectInputDto;
import com.zenika.zira.project.repository.ProjectRepository;
import com.zenika.zira.ticket.application.dto.TicketDto;
import com.zenika.zira.ticket.application.dto.TicketInputDto;
import com.zenika.zira.ticket.domain.TicketService;
import com.zenika.zira.web.exceptions.InvalidException;
import com.zenika.zira.web.exceptions.NotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;

@Service
public class ProjectService {

    private final ProjectRepository repository;
    private final TicketService ticketService;

    public ProjectService(ProjectRepository repository, TicketService ticketService) {
        this.repository = repository;
        this.ticketService = ticketService;
    }

    /**
     * get all projects filtered by name and/or authenticated user is the author
     * @param query searched name (optional)
     * @param isUserAuthor should user be project author (optional)
     * @return projects list
     */
    public List<ProjectDto> getAllProjects(String query, Boolean isUserAuthor) {
        if (!query.isBlank() && isUserAuthor) {
            User author = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return ProjectDto.fromDomain(this.repository.findAllByNameAndAuthor(query, author.getUsername()));
        } else if (!query.isBlank()) {
            return ProjectDto.fromDomain(this.repository.findAllByName(query));
            // return ProjectDto.fromDomain(this.repository.findAllByNameIgnoreCaseContaining(query.get()));
        } else if (isUserAuthor) {
            User author = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return ProjectDto.fromDomain(this.repository.findAllByAuthor(author.getUsername()));
        } else {
            return ProjectDto.fromDomain(this.repository.findAllByOrderByCreatedAtDesc());
        }
    }

    public ProjectDto getProjectById(long id) {
        Project foundProject = this.repository.findById(id)
                .orElseThrow(() -> new NotFoundException(Project.class, id));
        return ProjectDto.fromDomain(foundProject);
    }

    public ProjectDto createProject(ProjectInputDto projectToAdd, String userName) {
        Project newProject = ProjectInputDto.toDomain(projectToAdd);
        newProject.setCreatedAt(Instant.now());
        newProject.setAuthor(userName);
        return ProjectDto.fromDomain(this.repository.save(newProject));
    }

    public ProjectDto updateProject(Long id, ProjectInputDto projectToUpdate) {
        Project existingProject = isProjectExists(id);
        existingProject.setName(projectToUpdate.getName());
        existingProject.setDescription(projectToUpdate.getDescription());
        return ProjectDto.fromDomain(this.repository.save(existingProject));
    }

    public void deleteProject(Long id) {
        Project project = isProjectExists(id);
        this.repository.delete(project);
    }

    @Transactional
    public TicketDto addTicket(Long id, TicketInputDto ticketToAdd, String userName) {
        Project existingProject = isProjectExists(id);
        return this.ticketService.createTicket(ticketToAdd, existingProject, userName);
    }

    /**
     * check if project exists
     * @param id id of searched project
     * @return found project or bad request exception
     */
    private Project isProjectExists(Long id) {
        return this.repository.findById(id)
                .orElseThrow(() -> new InvalidException("The provided id " + id + " is not valid"));
    }
}
