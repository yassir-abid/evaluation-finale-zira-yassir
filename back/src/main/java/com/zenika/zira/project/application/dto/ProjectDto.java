package com.zenika.zira.project.application.dto;

import com.zenika.zira.project.domain.Project;
import com.zenika.zira.ticket.application.dto.TicketDto;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.StreamSupport;

public class ProjectDto {

    public static ProjectDto fromDomain(Project project) {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setId(project.getId());
        projectDto.setName(project.getName());
        projectDto.setDescription(project.getDescription());
        projectDto.setCreatedAt(project.getCreatedAt());
        projectDto.setAuthor(project.getAuthor());
        projectDto.setTickets(TicketDto.fromDomain(project.getTickets()));
        return projectDto;
    }

    public static List<ProjectDto> fromDomain(Iterable<Project> ticketList) {
        return StreamSupport.stream(ticketList.spliterator(), false)
                .map(ProjectDto::fromDomain)
                .toList();
    }

    private long id;
    private String name;
    private String description;
    private Instant createdAt;
    private String author;
    private List<TicketDto> tickets = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public List<TicketDto> getTickets() {
        return tickets;
    }

    public void setTickets(List<TicketDto> tickets) {
        this.tickets = tickets;
    }
}
