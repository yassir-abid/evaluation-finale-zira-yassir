package com.zenika.zira.web;

import com.zenika.zira.web.dto.ErrorDto;
import com.zenika.zira.web.exceptions.InvalidException;
import com.zenika.zira.web.exceptions.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionsHandler {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorDto> onNotFoundException(NotFoundException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ErrorDto(e.getMessage()));
    }

    @ExceptionHandler(InvalidException.class)
    public ResponseEntity<ErrorDto> onInvalidException(InvalidException e) {
        return ResponseEntity.badRequest()
                .body(new ErrorDto(e.getMessage()));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    ResponseEntity<ErrorDto> InvalidArgumentError(MethodArgumentNotValidException e) {
        final FieldError fieldError = e.getFieldError();
        assert fieldError != null;
        return ResponseEntity
                .badRequest()
                .body(new ErrorDto(fieldError.getField() + " " + fieldError.getDefaultMessage()));
    }
}
