package com.zenika.zira.web.exceptions;

public class NotFoundException extends RuntimeException {
    private final Long id;
    private final Class<?> entityClass;

    public NotFoundException(Class<?> entityClass, Long id) {
        super(entityClass.getSimpleName() + " with identifier " + id + " is not found");
        this.id = id;
        this.entityClass = entityClass;
    }

    public NotFoundException(Class<?> entityClass, Long id, Throwable cause) {
        super(entityClass.getSimpleName() + " with identifier " + id + " is not found", cause);
        this.id = id;
        this.entityClass = entityClass;
    }

    public Long getId() {
        return id;
    }

    public Class<?> getEntityClass() {
        return entityClass;
    }
}
