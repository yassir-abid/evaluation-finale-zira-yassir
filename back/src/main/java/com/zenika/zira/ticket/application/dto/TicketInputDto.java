package com.zenika.zira.ticket.application.dto;

import com.zenika.zira.ticket.domain.Ticket;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class TicketInputDto {
    public static Ticket toDomain(TicketInputDto ticketInputDto) {
        Ticket ticket = new Ticket();
        ticket.setTitle(ticketInputDto.getTitle());
        ticket.setContent(ticketInputDto.getContent());
        ticket.setStatus(ticketInputDto.getStatus().name());
        return ticket;
    }

    @NotBlank
    @Size(max = 50)
    private String title;

    @NotBlank
    @Size(max = 1000)
    private String content;

    @NotNull
    private Status status;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
