package com.zenika.zira.ticket.application.dto;

import com.zenika.zira.ticket.domain.Ticket;

import java.time.Instant;
import java.util.List;
import java.util.stream.StreamSupport;

public class TicketDto {

    public static TicketDto fromDomain(Ticket ticket) {
        TicketDto ticketDto = new TicketDto();
        ticketDto.setId(ticket.getId());
        ticketDto.setTitle(ticket.getTitle());
        ticketDto.setContent(ticket.getContent());
        ticketDto.setStatus(ticket.getStatus());
        ticketDto.setCreatedAt(ticket.getCreatedAt());
        ticketDto.setAuthor(ticket.getAuthor());
        return ticketDto;
    }

    public static List<TicketDto> fromDomain(Iterable<Ticket> ticketList) {
        return StreamSupport.stream(ticketList.spliterator(), false)
                .map(TicketDto::fromDomain)
                .toList();
    }

    private long id;
    private String title;
    private String content;
    private String status;
    private Instant createdAt;
    private String author;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
