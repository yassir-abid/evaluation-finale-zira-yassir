package com.zenika.zira.ticket.application;

import com.zenika.zira.ticket.application.dto.TicketDto;
import com.zenika.zira.ticket.application.dto.TicketInputDto;
import com.zenika.zira.ticket.domain.TicketService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/tickets")
@SecurityRequirement(name = "basicAuth")
public class TicketController {

    private static final Logger logger = LoggerFactory.getLogger(TicketController.class);

    private final TicketService service;

    public TicketController(TicketService service) {
        this.service = service;
    }

    /**
     * get one ticket by id
     * @param id ticket id
     * @return ticket or not found exception
     */
    @GetMapping("/{id}")
    TicketDto getTicketById(@PathVariable("id") final long id) {
        logger.info("getTicketById");
        return this.service.getTicketById(id);
    }

    /**
     * update existing ticket
     * @param id ticket id to update
     * @param ticket ticket infos
     * @return Ticket updated
     */
    @PutMapping("/{id}")
    TicketDto updateTicket(@PathVariable Long id, @RequestBody @Validated TicketInputDto ticket) {
        logger.info("updateTicket");
        return this.service.updateTicket(id, ticket);
    }

    /**
     * delete ticket
     * @param id ticket id to delete
     */
    @DeleteMapping("/{id}")
    void deleteTicket(@PathVariable Long id) {
        logger.info("deleteTicket");
        this.service.deleteTicket(id);
    }
}
