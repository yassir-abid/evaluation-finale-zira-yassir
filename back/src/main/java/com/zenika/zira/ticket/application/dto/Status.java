package com.zenika.zira.ticket.application.dto;

public enum Status {
    TODO,
    INPROGRESS,
    DONE;
}
