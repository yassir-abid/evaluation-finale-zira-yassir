package com.zenika.zira.ticket.domain;

import com.zenika.zira.project.domain.Project;
import com.zenika.zira.web.exceptions.InvalidException;
import jakarta.persistence.*;
import org.springframework.util.StringUtils;

import java.time.Instant;

import static jakarta.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "ticket")
public class Ticket {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private long id;

    @Column(name = "title")
    private String title;

    @Column(name = "content")
    private String content;

    @Column(name = "status")
    private String status;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "author")
    private String author;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if (!StringUtils.hasText(title)) {
            throw new InvalidException("Title is empty");
        }
        if (title.length() > 50) {
            throw new InvalidException("Title length must be less than 50 characters");
        }
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        if (!StringUtils.hasText(content)) {
            throw new InvalidException("Content is empty");
        }
        if (content.length() > 1000) {
            throw new InvalidException("Content length must be less than 1000 characters");
        }
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        if (!StringUtils.hasText(status)) {
            throw new InvalidException("Status is empty");
        }
        this.status = status;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
