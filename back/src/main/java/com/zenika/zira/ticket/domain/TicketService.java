package com.zenika.zira.ticket.domain;

import com.zenika.zira.project.domain.Project;
import com.zenika.zira.ticket.application.dto.TicketDto;
import com.zenika.zira.ticket.application.dto.TicketInputDto;
import com.zenika.zira.ticket.repository.TicketRepository;
import com.zenika.zira.web.exceptions.InvalidException;
import com.zenika.zira.web.exceptions.NotFoundException;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class TicketService {

    private final TicketRepository repository;

    public TicketService(TicketRepository repository) {
        this.repository = repository;
    }

    public TicketDto getTicketById(long id) {
        Ticket foundTicket = this.repository.findById(id)
                .orElseThrow(() -> new NotFoundException(Ticket.class, id));
        return TicketDto.fromDomain(foundTicket);
    }

    public TicketDto createTicket(TicketInputDto ticket, Project project, String userName) {
       Ticket newTicket = TicketInputDto.toDomain(ticket);
       newTicket.setCreatedAt(Instant.now());
       newTicket.setProject(project);
       newTicket.setAuthor(userName);
       return TicketDto.fromDomain(this.repository.save(newTicket));
    }

    public TicketDto updateTicket(Long id, TicketInputDto ticket) {
        Ticket existingTicket = isTicketExists(id);
        existingTicket.setTitle(ticket.getTitle());
        existingTicket.setContent(ticket.getContent());
        existingTicket.setStatus(ticket.getStatus().name());
        return TicketDto.fromDomain(this.repository.save(existingTicket));
    }

    public void deleteTicket(Long id) {
        Ticket ticket = isTicketExists(id);
        this.repository.delete(ticket);
    }

    /**
     * check if ticket exists
     * @param id id of searched ticket
     * @return found ticket or bad request exception
     */
    private Ticket isTicketExists(Long id) {
        return this.repository.findById(id)
                .orElseThrow(() -> new InvalidException("The provided id " + id + " is not valid"));
    }
}
