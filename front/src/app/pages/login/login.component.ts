import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  errors: string[] = [];
  returnUrl: Params | string = {};

  constructor(
    private route: ActivatedRoute, 
    private router: Router, 
    private _authenticationService: AuthenticationService) {  }

  form = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  })

  ngOnInit() {
    this.form.reset();
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  submit() {
    this.errors = [];

    if (this.form.valid) {
      const values = this.form.value as User;
      this._authenticationService.login({...values});
      this.router.navigate([this.returnUrl]);
    }

    Object.entries(this.form.controls).forEach(([key, control]) => {
      if (control.errors) {
        if (Object.hasOwn(control.errors, 'required')) {
          this.errors.push(`Champ ${key} : le champ est obligatoire`)
        }
        if (Object.hasOwn(control.errors, 'maxlength')) {
          this.errors.push(`Champ ${key} : la valeur ne doit pas avoir plus de ${control.errors['maxlength'].requiredLength} caractères`)
        }
        if (Object.hasOwn(control.errors, 'pattern')) {
          this.errors.push(`Champ ${key} : le statut indiqué n'est pas valide`)
        }
      }
    })
  }
}
