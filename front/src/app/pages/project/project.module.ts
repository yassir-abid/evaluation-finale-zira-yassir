import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectRoutingModule } from './project-routing.module';
import { DisplayProjectComponent } from './display-project/display-project.component';
import { ProjectComponent } from './project.component';
import { TicketCardModule } from 'src/app/components/ticket-card/ticket-card.module';
import { UpdateProjectComponent } from './update-project/update-project.component';
import { AddProjectComponent } from './add-project/add-project.component';
import { ProjectFormModule } from 'src/app/components/project-form/project-form.module';
import { AddTicketComponent } from './add-ticket/add-ticket.component';
import { TicketFormModule } from 'src/app/components/ticket-form/ticket-form.module';



@NgModule({
  declarations: [
    ProjectComponent,
    DisplayProjectComponent,
    UpdateProjectComponent,
    AddProjectComponent,
    AddTicketComponent,
  ],
  imports: [
    CommonModule,
    ProjectRoutingModule,
    TicketCardModule,
    ProjectFormModule,
    TicketFormModule,
  ]
})
export class ProjectModule { }
