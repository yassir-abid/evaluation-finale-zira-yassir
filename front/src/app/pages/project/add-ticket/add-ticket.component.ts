import { Component } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Project, TicketDto } from 'src/app/models/api';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-add-ticket',
  templateUrl: './add-ticket.component.html',
  styleUrls: ['./add-ticket.component.scss']
})
export class AddTicketComponent {

  id: string = '';
  project?: Project;
  
  constructor(private _projectService: ProjectService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    const snapshot: ActivatedRouteSnapshot = this.route.snapshot;
    this.id = snapshot.params['id'];
    this._projectService.getById(this.id).subscribe((project) => this.project = project);
  }

  handleCreateTicket(ticket: TicketDto): void {
    this._projectService.addTicket(ticket, this.id).subscribe(() => this.router.navigateByUrl(`/projects/${this.id}`));
  }

}
