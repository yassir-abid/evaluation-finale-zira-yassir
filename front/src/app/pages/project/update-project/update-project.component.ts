import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Project, ProjectDto } from 'src/app/models/api';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-update-project',
  templateUrl: './update-project.component.html',
  styleUrls: ['./update-project.component.scss']
})
export class UpdateProjectComponent implements OnInit {

  id: string = '';
  project?: Project;
  
  constructor(private _projectService: ProjectService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    const snapshot: ActivatedRouteSnapshot = this.route.snapshot;
    this.id = snapshot.params['id'];
    this._projectService.getById(this.id).subscribe((project) => this.project = project);
  }

  handleUpdateProject(project: ProjectDto): void {
    this._projectService.update(project, this.id).subscribe(project => this.router.navigateByUrl(`/projects/` + this.project?.id ));
  }
}
