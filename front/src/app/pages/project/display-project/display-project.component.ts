import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, Router} from '@angular/router';
import { Project } from 'src/app/models/api';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-display-project',
  templateUrl: './display-project.component.html',
  styleUrls: ['./display-project.component.scss']
})
export class DisplayProjectComponent implements OnInit {
  project?: Project;

  constructor(
    private _projectService: ProjectService, 
    private _authenticationService: AuthenticationService, 
    private route: ActivatedRoute, 
    private router: Router) {}

  ngOnInit(): void {
    const snapshot: ActivatedRouteSnapshot = this.route.snapshot;
    const id = snapshot.params['id'];
    this._projectService.getById(id).subscribe((project) => this.project = project);
  }

  handleEditButtonClick() {
    this.router.navigateByUrl('/projects/' + this.project?.id + '/update');
  }

  handleAddTicketButtonClick() {
    this.router.navigateByUrl('/projects/' + this.project?.id + '/addticket');
  }

  handleDeleteButtonClick() {
    this._authenticationService.isLoggedIn().subscribe((isLoggedIn) => {
      if(!isLoggedIn) {
        this.router.navigateByUrl('/login');
        return;
      }
      if (this.project)  {
        this._projectService.delete(this.project.id).subscribe(() => this.router.navigateByUrl(`/`));
      }
    })
  }

  handleDeleteTicket(id: number) {
    if (this.project) {
      this.project.tickets = this.project?.tickets.filter((ticket) => ticket.id !== id);
    }
  }
}
