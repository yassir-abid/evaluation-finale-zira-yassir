import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectDto } from 'src/app/models/api';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.scss']
})
export class AddProjectComponent {

  constructor(private _projectService: ProjectService, private router: Router) { }

  handleCreateProject(project: ProjectDto): void {
    this._projectService.create(project).subscribe(project => this.router.navigateByUrl(`/`));
  }
}
