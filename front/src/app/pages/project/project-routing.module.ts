import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import { ProjectComponent } from './project.component';
import { DisplayProjectComponent } from './display-project/display-project.component';
import { UpdateProjectComponent } from './update-project/update-project.component';
import { AddProjectComponent } from './add-project/add-project.component';
import { AddTicketComponent } from './add-ticket/add-ticket.component';
import { isAuthenticated } from 'src/app/guards/isAuthenticated';

const routes: Routes = [{
  path: '', component: ProjectComponent, children: [
    { path: 'add', component: AddProjectComponent, canActivate: [isAuthenticated]},
    { path: ':id/update', component: UpdateProjectComponent, canActivate: [isAuthenticated]},
    { path: ':id/addticket', component: AddTicketComponent, canActivate: [isAuthenticated]},
    { path: ':id', component: DisplayProjectComponent},
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectRoutingModule { }
