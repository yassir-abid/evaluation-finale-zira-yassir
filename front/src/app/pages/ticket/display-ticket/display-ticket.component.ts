import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, Router, Params} from '@angular/router';
import { Ticket } from 'src/app/models/api';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { TicketService } from 'src/app/services/ticket.service';

@Component({
  selector: 'app-display-ticket',
  templateUrl: './display-ticket.component.html',
  styleUrls: ['./display-ticket.component.scss']
})
export class DisplayTicketComponent implements OnInit{
 ticket?: Ticket;
 projectId!: string;

 constructor(
  private _ticketService: TicketService,
  private _authenticationService: AuthenticationService,
  private route: ActivatedRoute,
  private router: Router) {}

  ngOnInit(): void {
    const snapshot: ActivatedRouteSnapshot = this.route.snapshot;
    const id = snapshot.params['id'];
    this._ticketService.getById(id).subscribe((ticket) => this.ticket = ticket);

    this.projectId = this._ticketService.projectId;
  }

  handleReturnButtonClick() {
    this.router.navigateByUrl('/projects/' + this.projectId);
  }

  handleEditButtonClick() {
    this.router.navigateByUrl('/projects/' + this.projectId + '/tickets/' + this.ticket?.id + '/update');
  }

  handleDeleteButtonClick() {
    this._authenticationService.isLoggedIn().subscribe((isLoggedIn) => {
      if(!isLoggedIn) {
        this.router.navigateByUrl('/login');
        return;
      }
      if (this.ticket)  {
        this._ticketService.delete(this.ticket.id).subscribe(() => this.router.navigateByUrl('/projects/' + this.projectId));
      }
    })
  }
}
