import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import { TicketComponent } from './ticket.component';
import { DisplayTicketComponent } from './display-ticket/display-ticket.component';
import { UpdateTicketComponent } from './update-ticket/update-ticket.component';
import { isAuthenticated } from 'src/app/guards/isAuthenticated';

const routes: Routes = [
  { path: '', component: TicketComponent, children: [
    { path: ':id/update', component: UpdateTicketComponent, canActivate: [isAuthenticated]},
    { path: ':id', component: DisplayTicketComponent},
  ]}
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TicketRoutingModule { }
