import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, Router, Params} from '@angular/router';
import { Ticket } from 'src/app/models/api';
import { TicketService } from 'src/app/services/ticket.service';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss']
})
export class TicketComponent implements OnInit {

  projectId!: string;

  constructor(private _ticketService: TicketService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    const snapshot: ActivatedRouteSnapshot = this.route.snapshot;
    this.projectId = snapshot.params['id'];
    this._ticketService.projectId = this.projectId;
  }
}
