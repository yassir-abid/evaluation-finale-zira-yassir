import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Ticket, TicketDto } from 'src/app/models/api';
import { TicketService } from 'src/app/services/ticket.service';

@Component({
  selector: 'app-update-ticket',
  templateUrl: './update-ticket.component.html',
  styleUrls: ['./update-ticket.component.scss']
})
export class UpdateTicketComponent implements OnInit {
  id: string = '';
  ticket?: Ticket;
  projectId!: string;

  constructor(private _ticketService: TicketService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    const snapshot: ActivatedRouteSnapshot = this.route.snapshot;
    this.id = snapshot.params['id'];
    this._ticketService.getById(this.id).subscribe((ticket) => this.ticket = ticket);

    this.projectId = this._ticketService.projectId;
  }

  handleUpdateTicket(ticket: TicketDto): void {
    this._ticketService.update(ticket, this.id).subscribe(() => this.router.navigateByUrl('/projects/' + this.projectId));
  }

  handleReturnButtonClick() {
    this.router.navigateByUrl('/projects/' + this.projectId);
  }
}
