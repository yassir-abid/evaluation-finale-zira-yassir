import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicketRoutingModule } from './ticket-routing.module';
import { TicketComponent } from './ticket.component';
import { DisplayTicketComponent } from './display-ticket/display-ticket.component';
import { UpdateTicketComponent } from './update-ticket/update-ticket.component';
import { TicketFormModule } from 'src/app/components/ticket-form/ticket-form.module';



@NgModule({
  declarations: [
    TicketComponent,
    DisplayTicketComponent,
    UpdateTicketComponent,
  ],
  imports: [
    CommonModule,
    TicketRoutingModule,
    TicketFormModule,
  ]
})
export class TicketModule { }
