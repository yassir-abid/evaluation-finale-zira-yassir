import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsComponent } from './projects.component';
import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectCardModule } from 'src/app/components/project-card/project-card.module';
import { SearchModule } from 'src/app/components/search/search.module';


@NgModule({
  declarations: [ProjectsComponent],
  imports: [
    CommonModule,
    ProjectsRoutingModule,
    ProjectCardModule,
    SearchModule
  ]
})
export class ProjectsModule { }
