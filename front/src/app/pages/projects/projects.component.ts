import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/api';
import { SearchFields } from 'src/app/models/search';
import { ProjectService } from 'src/app/services/project.service';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
  projects: Project[] = [];

  constructor(private _projectService: ProjectService, private _searchService: SearchService) { }

  ngOnInit(): void {
    this._projectService.getAll().subscribe((projects) => this.projects = projects);
  }

  handleSearchInputsChange(searchFields: SearchFields) {
    this._searchService.setQuery(searchFields.query);
    this._searchService.setIsUserAuthor(searchFields.isUserAuthor);
    this._projectService.getAll().subscribe((projects) => this.projects = projects);
  }

  handleDeleteProject(id: number) {
    this._projectService.delete(id).subscribe(() => {
      this.projects = this.projects.filter(project => project.id !== id);
    });
  }
}
