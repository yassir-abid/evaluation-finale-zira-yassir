import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'

const routes: Routes = [
  { path: '', loadChildren: () => import('./pages/projects/projects.module').then((m) => m.ProjectsModule) },
  { path: 'projects', loadChildren: () => import('./pages/project/project.module').then((m) => m.ProjectModule) },
  { path: 'projects/:id/tickets', loadChildren: () => import('./pages/ticket/ticket.module').then((m) => m.TicketModule) },
  { path: 'login', loadChildren: () => import('./pages/login/login.module').then((m) => m.LoginModule) },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
