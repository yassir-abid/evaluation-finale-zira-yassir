import { AuthenticationService } from "../services/authentication.service";
import { inject } from "@angular/core";
import { Router } from "@angular/router";

export const isAuthenticated = () => {
    const authenticationService = inject(AuthenticationService);
    const router = inject(Router);
    let isLogged: boolean = false
    authenticationService.isLoggedIn().subscribe((isLoggedIn) => {
        if(!isLoggedIn) {
            router.navigateByUrl('/login')
            isLogged = false;
        } else {
            isLogged = true;
        }
    })
    return isLogged;
}