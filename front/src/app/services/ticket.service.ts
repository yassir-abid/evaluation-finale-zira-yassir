import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, tap } from 'rxjs';
import { Ticket, TicketDto } from '../models/api';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  private API_URL = '/api/tickets';

  projectId!: string;

  constructor(private _httpClient: HttpClient) {}

  getById(id: string): Observable<Ticket> {
    return this._httpClient.get<Ticket>(this.API_URL + '/' + id);
  }
  
  update(ticketToUpdate: TicketDto, id: string): Observable<Ticket> {
    return this._httpClient.put<Ticket>(`${this.API_URL}/${id}`, ticketToUpdate);
  }

  delete(id: number): Observable<void> {
    return this._httpClient.delete<void>(`${this.API_URL}/${id}`);
  }
}
