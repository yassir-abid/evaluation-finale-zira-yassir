import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private _query: string = "";
  private _isUserAuthor: boolean = false;

  constructor() { }

  public getQuery(): string {
    return this._query;
  }

  public setQuery(query: string) {
    this._query = query;
  }

  public getIsUserAuthor(): boolean {
    return this._isUserAuthor;
  }

  public setIsUserAuthor(isUserAuthor: boolean) {
    this._isUserAuthor = isUserAuthor;
  }


}
