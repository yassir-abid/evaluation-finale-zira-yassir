import { TestBed } from '@angular/core/testing';

import { ProjectService } from './project.service';
import { Project, ProjectDto } from '../models/api';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { SearchService } from './search.service';

describe('ProjectService', () => {
    let service: ProjectService;
    let httpClient: HttpClient;
    let searchService: SearchService;
    let project: ProjectDto;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientModule],
            providers: [
                HttpClient
            ]
        })
        TestBed.configureTestingModule({});
        service = TestBed.inject(ProjectService);
        httpClient = TestBed.inject(HttpClient);
        searchService = TestBed.inject(SearchService);
        project = {
            name: 'Kaamelott',
            description: 'API pour récupérer des citations aléatoires',
        }
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should have an empty projects list when it is initialized', () => {
        expect(service.projects.length).toBe(0);
    });

    // describe('addProject', () => {
    //     it('should add project to API', () => {
    //         service.create(project).subscribe(() => {
    //             expect(service.projects.length).toBe(1);
    //             expect(service.projects[0].name).toEqual('Kaamelott');
    //         });
    //     });
    // });

    // describe('getProjectById', () => {
    //     it('should get project by id from api', () => {
    //         service.create(project).subscribe((project) => {
    //             expect(service.projects.length).toBe(1);
    //             service.getById(project.id.toString()).subscribe((foundProject) => {
    //                 expect(service.projects[0].name).toEqual(foundProject?.name);
    //             });
    //         });
    //     });
    // });

});
