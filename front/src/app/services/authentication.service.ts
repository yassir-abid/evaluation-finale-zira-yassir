import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private readonly LOCAL_STORAGE_KEY = 'currentUser';

  isLoginSubject = new BehaviorSubject<boolean>(this.isLogged());

  isLogged(): boolean {
    return !!localStorage.getItem(this.LOCAL_STORAGE_KEY);
  }

  login(user: User) {
    localStorage.setItem(this.LOCAL_STORAGE_KEY, JSON.stringify(user));
    this.isLoginSubject.next(true);
  }

  logout(): void {
    localStorage.removeItem(this.LOCAL_STORAGE_KEY);
    this.isLoginSubject.next(false);
  }

  isLoggedIn() : Observable<boolean> {
    return this.isLoginSubject.asObservable();
  }

  getCurrentUserBasicAuthentication(): string | undefined {
    const currentUserPlain = localStorage.getItem(this.LOCAL_STORAGE_KEY)
    if (currentUserPlain) {
      const currentUser = JSON.parse(currentUserPlain)
      return "Basic " + btoa(currentUser.username + ":" + currentUser.password);
    } else {
      return undefined;
    }
  }
}
