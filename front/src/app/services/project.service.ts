import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, tap } from 'rxjs';
import { Project, ProjectDto, Ticket, TicketDto } from '../models/api';
import { SearchService } from './search.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private API_URL = '/api/projects';

  projects: Project[] = [];

  constructor(private _httpClient: HttpClient, private _searchService: SearchService) { }

  getAll(): Observable<Project[]> {
    const query = this._searchService.getQuery();
    const isUserAuthor = this._searchService.getIsUserAuthor();
    let url: string = this.API_URL;
    if (query && isUserAuthor) {
      url = url + `?query=${query}&isUserAuthor=${isUserAuthor}`;
    } else if (query) {
      url = url + `?query=${query}`;
    } else if (isUserAuthor) {
      url = url + `?isUserAuthor=${isUserAuthor}`;
    }
    return this._httpClient.get<Project[]>(url).pipe(
      tap((projects) => this.projects = projects)
    );;
  }

  getById(id: string): Observable<Project> {
    return this._httpClient.get<Project>(`${this.API_URL}/${id}`);
  }

  create(projectToAdd: ProjectDto): Observable<Project> {
    return this._httpClient.post<Project>(this.API_URL, projectToAdd).pipe(
      tap((project) => {
        this.projects.push(project);
      })
    );
  }

  update(projectToUpdate: ProjectDto, id: string): Observable<Project> {
    return this._httpClient.put<Project>(`${this.API_URL}/${id}`, projectToUpdate);
  }

  delete(id: number): Observable<void> {
    return this._httpClient.delete<void>(`${this.API_URL}/${id}`);
  }

  addTicket(ticket: TicketDto, id: string): Observable<Ticket> {
    return this._httpClient.post<Ticket>(`${this.API_URL}/${id}/tickets`, ticket);
  }
}
