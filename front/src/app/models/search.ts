export interface SearchFields {
    query: string;
    isUserAuthor: boolean;
}