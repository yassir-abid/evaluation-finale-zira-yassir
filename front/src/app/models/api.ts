export interface Ticket {
    id: number;
    title: string;
    content: string;
    status: string;
    author: string;
    createdAt: string;
}

export interface TicketDto {
    title: string;
    content: string;
    status: string;
}

export interface Project {
    id: number;
    name: string;
    description: string;
    createdAt: string;
    author: string;
    tickets: Ticket[];
}

export interface ProjectDto {
    name: string;
    description: string;
}