import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Project } from 'src/app/models/api';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-project-card',
  templateUrl: './project-card.component.html',
  styleUrls: ['./project-card.component.scss']
})
export class ProjectCardComponent {
  @Input()
  public project!: Project;

  @Output()
  deleteProject = new EventEmitter<number>();

  constructor(private _projectService: ProjectService, private _authenticationService: AuthenticationService, private router: Router) {}

  handleDetailsButtonClick() {
    this.router.navigateByUrl('/projects/' + this.project.id);
  }

  handleEditButtonClick() {
    this.router.navigateByUrl('/projects/' + this.project.id + '/update');
  }

  handleDeleteButtonClick() {
    this._authenticationService.isLoggedIn().subscribe((isLoggedIn) => {
      if(!isLoggedIn) {
        this.router.navigateByUrl('/login');
        return;
      }
      this.deleteProject.emit(this.project.id);
    })
  }
}
