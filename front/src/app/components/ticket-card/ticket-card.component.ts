import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Ticket } from 'src/app/models/api';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { TicketService } from 'src/app/services/ticket.service';

@Component({
  selector: 'app-ticket-card',
  templateUrl: './ticket-card.component.html',
  styleUrls: ['./ticket-card.component.scss']
})
export class TicketCardComponent {

  @Input()
  public ticket!: Ticket;

  @Input()
  public projectId?: number;

  @Output()
  deleteTicket = new EventEmitter<number>();

  constructor(
    private _ticketService: TicketService, 
    private _authenticationService: AuthenticationService, 
    private router: Router) {}

  handleDetailsButtonClick() {
    this.router.navigateByUrl('/projects/' + this.projectId + '/tickets/' + this.ticket.id);
  }

  handleEditButtonClick() {
    this.router.navigateByUrl('/projects/' + this.projectId + '/tickets/' + this.ticket.id + '/update');
  }

  handleDeleteButtonClick() {
    this._authenticationService.isLoggedIn().subscribe((isLoggedIn) => {
      if(!isLoggedIn) {
        this.router.navigateByUrl('/login');
        return;
      }
      this._ticketService.delete(this.ticket.id).subscribe(() => {
        this.deleteTicket.emit(this.ticket.id);
        this.router.navigateByUrl('/projects/' + this.projectId);
      });
    })
  }
}
