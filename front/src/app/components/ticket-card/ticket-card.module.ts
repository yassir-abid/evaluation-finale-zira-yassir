import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicketCardComponent } from './ticket-card.component';



@NgModule({
  declarations: [TicketCardComponent],
  imports: [
    CommonModule
  ],
  exports: [TicketCardComponent]
})
export class TicketCardModule { }
