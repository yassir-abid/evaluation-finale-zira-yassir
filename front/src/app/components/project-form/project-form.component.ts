import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Project, ProjectDto } from 'src/app/models/api';

@Component({
  selector: 'app-project-form',
  templateUrl: './project-form.component.html',
  styleUrls: ['./project-form.component.scss']
})
export class ProjectFormComponent {

  @Input()
  project: Project | undefined;

  @Output()
  createProject = new EventEmitter<ProjectDto>();

  @Output()
  updateProject = new EventEmitter<ProjectDto>();

  errors: string[] = [];

  form = new FormGroup({
    name: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required, Validators.maxLength(500)]),
  })

  ngOnChanges(): void {
    if (this.project != undefined) {
      this.form.patchValue({
        name: this.project.name,
        description: this.project.description,
      });
    }
  }

  submit() {
    this.errors = [];

    if (this.form.valid) {
      const values = this.form.value as ProjectDto;
      if(this.project) {
        this.updateProject.emit({...values});
      } else {
        this.createProject.emit({...values});
      }
    } 

    Object.entries(this.form.controls).forEach(([key, control]) => {
      if (control.errors) {
        if (Object.hasOwn(control.errors, 'required')) {
          this.errors.push(`Champ ${key} : le champ est obligatoire`)
        }
        if (Object.hasOwn(control.errors, 'maxlength')) {
          this.errors.push(`Champ ${key} : la valeur ne doit pas avoir plus de ${control.errors['maxlength'].requiredLength} caractères`)
        }
      }
    })
  }
}
