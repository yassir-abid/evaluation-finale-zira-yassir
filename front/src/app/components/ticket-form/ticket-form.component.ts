import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Ticket, TicketDto } from 'src/app/models/api';

@Component({
  selector: 'app-ticket-form',
  templateUrl: './ticket-form.component.html',
  styleUrls: ['./ticket-form.component.scss']
})
export class TicketFormComponent {

  @Input()
  ticket: Ticket | undefined;

  @Output()
  createTicket = new EventEmitter<TicketDto>();

  @Output()
  updateTicket = new EventEmitter<TicketDto>();

  errors: string[] = [];

  form = new FormGroup({
    title: new FormControl('', [Validators.required, Validators.maxLength(50)]),
    content: new FormControl('', [Validators.required, Validators.maxLength(1000)]),
    status: new FormControl('', [Validators.pattern(/^TODO|INPROGRESS|DONE$/)]),
  })

  ngOnChanges(): void {
    if (this.ticket != undefined) {
      this.form.patchValue({
        title: this.ticket.title,
        content: this.ticket.content,
        status: this.ticket.status,
      });
    }
  }

  submit() {
    this.errors = [];

    if (this.form.valid) {
      let values = this.form.value as TicketDto;
      if (!values.status) {
        values = {...values, status: 'TODO'};
      }
      if(this.ticket) {
        this.updateTicket.emit({...values});
      } else {
        this.createTicket.emit({...values});
      }
    }

    Object.entries(this.form.controls).forEach(([key, control]) => {
      if (control.errors) {
        if (Object.hasOwn(control.errors, 'required')) {
          this.errors.push(`Champ ${key} : le champ est obligatoire`)
        }
        if (Object.hasOwn(control.errors, 'maxlength')) {
          this.errors.push(`Champ ${key} : la valeur ne doit pas avoir plus de ${control.errors['maxlength'].requiredLength} caractères`)
        }
        if (Object.hasOwn(control.errors, 'pattern')) {
          this.errors.push(`Champ ${key} : le statut indiqué n'est pas valide`)
        }
      }
    })

  }
}
