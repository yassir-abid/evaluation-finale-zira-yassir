import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { TicketFormComponent } from './ticket-form.component';



@NgModule({
  declarations: [TicketFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
  ],
  exports: [TicketFormComponent]
})
export class TicketFormModule { }
