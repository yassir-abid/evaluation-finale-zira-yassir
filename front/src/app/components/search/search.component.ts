import { Component, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { SearchFields } from 'src/app/models/search';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {

  @Output()
  changeSearchInputs = new EventEmitter<SearchFields>();

  searchText: string = "";
  isLoggedIn: Observable<boolean>;

  constructor(public _authenticationService: AuthenticationService) {
    this.isLoggedIn = _authenticationService.isLoggedIn();
  }

  searchForm = new FormGroup({
    query: new FormControl(''),
    isUserAuthor: new FormControl(false)
  })

  handleChangeInput() {
    const values = this.searchForm.value as SearchFields;
    if (values.isUserAuthor) {
      this.isLoggedIn.subscribe((isLogged) => {
        if (!isLogged) {
          values.isUserAuthor = false;
        }
      })
    }
    this.changeSearchInputs.emit(values);
  }
}

