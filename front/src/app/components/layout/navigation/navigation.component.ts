import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {

  isLoggedIn : Observable<boolean>;

  constructor(public _authenticationService: AuthenticationService, private router: Router) {
    this.isLoggedIn = _authenticationService.isLoggedIn();
  }

  handleLogoutClick() {
    this._authenticationService.logout();
    this.router.navigateByUrl(`/`);
  }
}
